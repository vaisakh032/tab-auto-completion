#     A module that will help user to import tab auto-completion feature to
#     his/her program
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#      Author: Vaisakh Anand (gitlab: @vaisakh032)
#      repo: https://gitlab.com/vaisakh032/tab-auto-completion
# /*******************************************************************************
#  * tabCompletion.py
#  *******************
#   This module will help you to impart tab auto-complete feature into your program
#   details on how to use the program and what each of the functions do is cleary
#   described through the mardown file and through doc strings
#   More details regarding the package will found at the repo mention above or try
#   by typing help(tabCompletion.Tabcomplete) after import tabCompletion in python.
#  *********************************************************************************

from tabCompletion.tabCompletion import Tabcomplete
