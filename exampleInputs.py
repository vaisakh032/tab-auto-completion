#    A example script to depict the usage of the tabCompletion module
#    and the method to use it in his/her program
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#      Author: Vaisakh Anand (gitlab: @vaisakh032)
# /*******************************************************************************
#  * exampleInputs.py
#  *******************
#   This program will give a clear picture on how the module is going to work.
#   the usage of the required functions are depicted here.
#  *********************************************************************************



from tabCompletion import Tabcomplete
import json


if __name__ == "__main__":
    print("This is a sample program to showcase tab completion")
    choice = ""
    while choice != "q" :
        print("Enter \n\ta) To create a sample json file")
        choice = input("\tb) To test auto tab completion\n\tq) To quit\n\t\t>>> ").lower()
        if choice == "a":
            tempDict = { "fruits":["orange","apple","grapes","orange_new"], "car":["honda","toyota","bmw", "honda_new", "honda_new2"]}
            with open("temp.json", 'w+') as outfile:
                json.dump(tempDict, outfile)
            print("Sample dict written to 'temp.json' !")

        elif choice == "b":
            path = "temp.json"
            sampleObj = Tabcomplete(path)

            print("Loaded sample json file ")
            print("List of possible values (default) for fruits inlude 'orange','apple','grapes','orange_new'")
            print("So try typing the first character and press tab to complete or type a total new type and hit enter to save it ")
            displayText = "Enter Fruits>> "
            searchFor = "fruits"
            ip_test1 = sampleObj.getip(displayText, searchFor)
            # ip_test1 = sampleObj.getip(displayText, searchFor, dynamicUpdation = 'n' )  #dynamic updation of values is off
            print("Got input as: ", ip_test1)

            print("List of possible values (default) for car inlude 'honda','toyota','bmw' 'honda_new' 'honda_new2'")
            print("So try typing the first character and press tab to complete or type a total new type and hit enter to save it ")
            displayText = "Enter cars>> "
            searchFor = "car"
            ip_test2 = sampleObj.getip(displayText, searchFor)
            # ip_test2 = sampleObj.getip(displayText, searchFor, compulsoryInput = 'n') #compulsory input is turned off
            print("Got input as: ", ip_test2)
        else:
            exit(1)
