#    A example script to depict the usage of the tabCompletion module
#    and the method to use it in his/her program
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#      Author: Vaisakh Anand (gitlab: @vaisakh032)
# /*******************************************************************************
#  * exampleDirectory.py
#  *******************
#   This program will give an example on how to use the module to complete
#   directory paths in you programs
#  *********************************************************************************



from tabCompletion.tabCompletion import Tabcomplete
import os

def get_final_list_of_files(directory):
    """Method to get the list of files and folders in the specified directory
    Returns:
     -> list of file names and folder paths in the directory
    """
    global ignorelist
    global ignore_files
    final_list = []
    for subdir, dirs, files in os.walk(directory):
        subdir = subdir.replace(directory, '')
        for file in files:
            if subdir.startswith('/'):
                final_list.append(subdir+file)
            else:
                final_list.append('/'+subdir+file)

    return final_list

if __name__ == "__main__":
    directory = os.getcwd().rstrip('/')
    listOfFiles = get_final_list_of_files(directory) # os.listdir(directory)
    sampleDictionary = {"path":listOfFiles}
    print("fetched list of files from current working directory")
    sampleObj = Tabcomplete(sampleDictionary)
    ip = sampleObj.getip("Enter Directory>> ","path",'y')
    print("Entered Directory:",os.getcwd(),ip)
